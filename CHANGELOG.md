# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://git.nextop.cn/nextopf2e/admin-elements/compare/@nextop/admin-glue@1.0.2...@nextop/admin-glue@1.0.3) (2022-11-09)

**Note:** Version bump only for package @nextop/admin-glue





# 1.0.2

#### BreakChanges

#### Features

#### Enhancements

#### Bugfixes

# 1.0.1 (2021-07-19)

#### Enhancements
* add config: X_VERSION, isRelease

# 1.0.0 (2021-07-02)
