export let projectName = '';
export let adminName = 'Nextop Admin';
export let X_VERSION = 'NA';
export let X_BRANCH_NAME = '';
export let isRelease = false;

export let i18n = {};
export let eventBus = {};

export function setConfig(config = {}) {
  projectName = config.projectName ?? projectName;
  adminName = config.adminName ?? adminName;
  X_VERSION = config.X_VERSION ?? X_VERSION;
  X_BRANCH_NAME = config.X_BRANCH_NAME ?? X_BRANCH_NAME;
  isRelease = config.isRelease ?? isRelease;

  i18n = config.i18n ?? i18n;
  eventBus = config.eventBus ?? eventBus;
}
